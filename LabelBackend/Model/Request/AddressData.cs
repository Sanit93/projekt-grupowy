﻿using System.ComponentModel.DataAnnotations;

namespace LabelBackend.Model
{
    public class AddressData
    {
        public AddressData()
        {
            Street = null;
            HomeNumber = null;
            PostalCode = null;
            City = null;
            Country = null;
        }
        [Required]
        public string Street { get; set; }
        [Required]
        public string HomeNumber { get; set; }
        [Required]
        public string PostalCode { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
    }
}
