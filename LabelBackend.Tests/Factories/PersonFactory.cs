﻿using LabelBackend.Model;
using LabelBackend.Tests.Assemblers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LabelBackend.Tests.Factories
{
    static class PersonFactory
    {
        static PersonData PersonData;

        public static PersonData Create()
        {
            PersonData = new PersonData();
            PersonData.AddressData = AddressFactory.Create();
            PersonData.FirstName = "firstname";
            PersonData.LastName = "lastname";
            PersonData.PhoneNumber = "1111";

            return PersonData;
        }
    }
}
