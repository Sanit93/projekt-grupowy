﻿using LabelBackend.Model;
using LabelBackend.Service.Pdf;
using NUnit.Framework;
using System.Text;
using LabelBackend.Tests.Factories;
using System;

namespace LabelBackend.Tests.Service.PdfTest
{
    public class PdfFactoryTest
    {

        [Test]
        public void CreateLabelTest()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            RequestData request = RequestFactory.Create();

            Console.WriteLine(request);
            
            PdfFactory pdfFactory = new PdfFactory(request);

            pdfFactory.CreateLabel();


            FileAssert.Exists("label.pdf");
        }

    }
}
