﻿using LabelBackend.Model;
using LabelBackend.Tests.Assemblers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LabelBackend.Tests.Factories
{
    class RequestFactory
    {
        static RequestData requestData;


        public static RequestData Create()
        {
            requestData = new RequestData();
            requestData.ReceiverData = PersonFactory.Create();
            requestData.SenderData = PersonFactory.Create();

            return requestData;
        }
    }
}
