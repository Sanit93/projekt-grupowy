﻿
using LabelBackend.Model;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class LabelAppClient : Form
    {
        public LabelAppClient()
        {
            InitializeComponent();
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            RequestData requestData = getRequestData();
            downloadLabel(requestData);
        }
        void downloadLabel(RequestData requestData)
        {
            var client = new RestClient("https://localhost:44345/api/label");
            var request = new RestRequest();
            request.AddJsonBody(requestData);
            var response = client.Post(request);
            if (!response.StatusCode.Equals(HttpStatusCode.OK))
            {
                MessageBox.Show(response.Content.ToString());
            } else
            {
                client.DownloadData(request).SaveAs("plik.pdf");
                openLabel();
            }
        }
        void openLabel()
        {
            
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "plik.pdf";
            Process.Start(startInfo);
        }

        RequestData getRequestData()
        {
            RequestData requestData = new RequestData();
            requestData.SenderData = getSenderData();
            requestData.ReceiverData = getReceiverData();
            return requestData;
        }

        PersonData getSenderData()
        {
            PersonData senderData = new PersonData();
            senderData.FirstName = senderFirstName.Text;
            senderData.LastName = senderLastName.Text;
            senderData.PhoneNumber = senderPhoneNumber.Text;
            senderData.AddressData.Street = senderStreet.Text;
            senderData.AddressData.HomeNumber = senderHomeNumber.Text;
            senderData.AddressData.PostalCode = senderPostalCode.Text;
            senderData.AddressData.City = senderCity.Text;
            senderData.AddressData.Country = senderCountry.Text;
            return senderData;

        }
        PersonData getReceiverData()
        {
            PersonData receiverData = new PersonData();
            receiverData.FirstName = receiverFirstName.Text;
            receiverData.LastName = receiverLastName.Text;
            receiverData.PhoneNumber = receiverPhoneNumber.Text;
            receiverData.AddressData.Street = receiverStreet.Text;
            receiverData.AddressData.HomeNumber = receiverHomeNumber.Text;
            receiverData.AddressData.PostalCode = receiverPostalCode.Text;
            receiverData.AddressData.City = receiverCity.Text;
            receiverData.AddressData.Country = receiverCountry.Text;
            return receiverData;
        }
    }
}
