﻿using NUnit.Framework;
using LabelBackend.Service.Barcode;
using System.Drawing;

namespace LabelBackend.Tests.Service.BarcodeTest
{
    public class BarcodeTest
    {
        
        [Test]
        public void GetImageTest()
        {
            Barcode barcode = new Barcode("1");

            Image img = barcode.GetImage();

            Assert.IsInstanceOf<Image>(img);
        }

        [Test]
        public void GetCodeTest()

        {
            string code = "1";
            Barcode barcode = new Barcode(code);

            string expectedCode = barcode.GetCode();

            Assert.AreEqual(expectedCode, code);
        }

    }
}
