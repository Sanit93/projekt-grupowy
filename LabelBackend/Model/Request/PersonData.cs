﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LabelBackend.Model
{
    public class PersonData
    {
        public PersonData()
        {
            FirstName = null;
            LastName = null; 
            AddressData = null;
            PhoneNumber = null;
        }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public AddressData AddressData { get; set; }
    }
}
