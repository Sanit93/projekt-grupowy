﻿using LabelBackend.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace LabelBackend.Tests.Assemblers
{
    class AddressFactory
    {
        static AddressData AddressData;

        public static AddressData Create()
        {
            AddressData = new AddressData();
            AddressData.City = "city";
            AddressData.Country = "country";
            AddressData.HomeNumber = "1";
            AddressData.PostalCode = "11-11";
            AddressData.Street = "street";

            return AddressData;
        }


    }
}
