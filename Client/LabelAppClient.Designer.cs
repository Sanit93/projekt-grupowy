﻿namespace Client
{
    partial class LabelAppClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.senderFirstName = new System.Windows.Forms.TextBox();
            this.senderLastName = new System.Windows.Forms.TextBox();
            this.senderPhoneNumber = new System.Windows.Forms.TextBox();
            this.senderStreet = new System.Windows.Forms.TextBox();
            this.senderHomeNumber = new System.Windows.Forms.TextBox();
            this.senderPostalCode = new System.Windows.Forms.TextBox();
            this.senderCity = new System.Windows.Forms.TextBox();
            this.senderCountry = new System.Windows.Forms.TextBox();
            this.receiverCountry = new System.Windows.Forms.TextBox();
            this.receiverCity = new System.Windows.Forms.TextBox();
            this.receiverPostalCode = new System.Windows.Forms.TextBox();
            this.receiverHomeNumber = new System.Windows.Forms.TextBox();
            this.receiverStreet = new System.Windows.Forms.TextBox();
            this.receiverPhoneNumber = new System.Windows.Forms.TextBox();
            this.receiverLastName = new System.Windows.Forms.TextBox();
            this.receiverFirstName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.generateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Imię nadawcy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwisko nadawcy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Telefon nadawcy";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ulica nadawcy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nr mieszkania nadawcy";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 231);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Kod pocztowy nadawcy";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Miasto nadawcy";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 309);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Państwo nadawcy";
            // 
            // senderFirstName
            // 
            this.senderFirstName.Location = new System.Drawing.Point(12, 52);
            this.senderFirstName.Name = "senderFirstName";
            this.senderFirstName.Size = new System.Drawing.Size(100, 20);
            this.senderFirstName.TabIndex = 8;
            // 
            // senderLastName
            // 
            this.senderLastName.Location = new System.Drawing.Point(12, 91);
            this.senderLastName.Name = "senderLastName";
            this.senderLastName.Size = new System.Drawing.Size(100, 20);
            this.senderLastName.TabIndex = 9;
            // 
            // senderPhoneNumber
            // 
            this.senderPhoneNumber.Location = new System.Drawing.Point(12, 130);
            this.senderPhoneNumber.Name = "senderPhoneNumber";
            this.senderPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.senderPhoneNumber.TabIndex = 10;
            // 
            // senderStreet
            // 
            this.senderStreet.Location = new System.Drawing.Point(12, 169);
            this.senderStreet.Name = "senderStreet";
            this.senderStreet.Size = new System.Drawing.Size(100, 20);
            this.senderStreet.TabIndex = 11;
            // 
            // senderHomeNumber
            // 
            this.senderHomeNumber.Location = new System.Drawing.Point(12, 208);
            this.senderHomeNumber.Name = "senderHomeNumber";
            this.senderHomeNumber.Size = new System.Drawing.Size(100, 20);
            this.senderHomeNumber.TabIndex = 12;
            // 
            // senderPostalCode
            // 
            this.senderPostalCode.Location = new System.Drawing.Point(12, 247);
            this.senderPostalCode.Name = "senderPostalCode";
            this.senderPostalCode.Size = new System.Drawing.Size(100, 20);
            this.senderPostalCode.TabIndex = 13;
            // 
            // senderCity
            // 
            this.senderCity.Location = new System.Drawing.Point(12, 286);
            this.senderCity.Name = "senderCity";
            this.senderCity.Size = new System.Drawing.Size(100, 20);
            this.senderCity.TabIndex = 14;
            // 
            // senderCountry
            // 
            this.senderCountry.Location = new System.Drawing.Point(12, 325);
            this.senderCountry.Name = "senderCountry";
            this.senderCountry.Size = new System.Drawing.Size(100, 20);
            this.senderCountry.TabIndex = 15;
            // 
            // receiverCountry
            // 
            this.receiverCountry.Location = new System.Drawing.Point(175, 325);
            this.receiverCountry.Name = "receiverCountry";
            this.receiverCountry.Size = new System.Drawing.Size(100, 20);
            this.receiverCountry.TabIndex = 31;
            // 
            // receiverCity
            // 
            this.receiverCity.Location = new System.Drawing.Point(175, 286);
            this.receiverCity.Name = "receiverCity";
            this.receiverCity.Size = new System.Drawing.Size(100, 20);
            this.receiverCity.TabIndex = 30;
            // 
            // receiverPostalCode
            // 
            this.receiverPostalCode.Location = new System.Drawing.Point(175, 247);
            this.receiverPostalCode.Name = "receiverPostalCode";
            this.receiverPostalCode.Size = new System.Drawing.Size(100, 20);
            this.receiverPostalCode.TabIndex = 29;
            // 
            // receiverHomeNumber
            // 
            this.receiverHomeNumber.Location = new System.Drawing.Point(175, 208);
            this.receiverHomeNumber.Name = "receiverHomeNumber";
            this.receiverHomeNumber.Size = new System.Drawing.Size(100, 20);
            this.receiverHomeNumber.TabIndex = 28;
            // 
            // receiverStreet
            // 
            this.receiverStreet.Location = new System.Drawing.Point(175, 169);
            this.receiverStreet.Name = "receiverStreet";
            this.receiverStreet.Size = new System.Drawing.Size(100, 20);
            this.receiverStreet.TabIndex = 27;
            // 
            // receiverPhoneNumber
            // 
            this.receiverPhoneNumber.Location = new System.Drawing.Point(175, 130);
            this.receiverPhoneNumber.Name = "receiverPhoneNumber";
            this.receiverPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.receiverPhoneNumber.TabIndex = 26;
            // 
            // receiverLastName
            // 
            this.receiverLastName.Location = new System.Drawing.Point(175, 91);
            this.receiverLastName.Name = "receiverLastName";
            this.receiverLastName.Size = new System.Drawing.Size(100, 20);
            this.receiverLastName.TabIndex = 25;
            // 
            // receiverFirstName
            // 
            this.receiverFirstName.Location = new System.Drawing.Point(175, 52);
            this.receiverFirstName.Name = "receiverFirstName";
            this.receiverFirstName.Size = new System.Drawing.Size(100, 20);
            this.receiverFirstName.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(175, 309);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Państwo odbiorcy";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(175, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Miasto odbiorcy";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(175, 231);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Kod pocztowy odbiorcy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(175, 192);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(116, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Nr mieszkania odbiorcy";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(175, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Ulica odbiorcy";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(175, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Telefon odbiorcy";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(175, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Nazwisko odbiorcy";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(175, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Imię odbiorcy";
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(106, 351);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(75, 23);
            this.generateButton.TabIndex = 32;
            this.generateButton.Text = "Start";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // LabelAppClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 395);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.receiverCountry);
            this.Controls.Add(this.receiverCity);
            this.Controls.Add(this.receiverPostalCode);
            this.Controls.Add(this.receiverHomeNumber);
            this.Controls.Add(this.receiverStreet);
            this.Controls.Add(this.receiverPhoneNumber);
            this.Controls.Add(this.receiverLastName);
            this.Controls.Add(this.receiverFirstName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.senderCountry);
            this.Controls.Add(this.senderCity);
            this.Controls.Add(this.senderPostalCode);
            this.Controls.Add(this.senderHomeNumber);
            this.Controls.Add(this.senderStreet);
            this.Controls.Add(this.senderPhoneNumber);
            this.Controls.Add(this.senderLastName);
            this.Controls.Add(this.senderFirstName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "LabelAppClient";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox senderFirstName;
        private System.Windows.Forms.TextBox senderLastName;
        private System.Windows.Forms.TextBox senderPhoneNumber;
        private System.Windows.Forms.TextBox senderStreet;
        private System.Windows.Forms.TextBox senderHomeNumber;
        private System.Windows.Forms.TextBox senderPostalCode;
        private System.Windows.Forms.TextBox senderCity;
        private System.Windows.Forms.TextBox senderCountry;
        private System.Windows.Forms.TextBox receiverCountry;
        private System.Windows.Forms.TextBox receiverCity;
        private System.Windows.Forms.TextBox receiverPostalCode;
        private System.Windows.Forms.TextBox receiverHomeNumber;
        private System.Windows.Forms.TextBox receiverStreet;
        private System.Windows.Forms.TextBox receiverPhoneNumber;
        private System.Windows.Forms.TextBox receiverLastName;
        private System.Windows.Forms.TextBox receiverFirstName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button generateButton;
    }
}

