﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LabelBackend.Model
{
    public class RequestData
    {
        public PersonData SenderData { get; set; }
        public PersonData ReceiverData { get; set; }
    }
}
