﻿using System.IO;
using LabelBackend.Model;
using LabelBackend.Service.Pdf;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace LabelBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LabelController : ControllerBase
    {
        // POST api/label
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] RequestData request)
        {
            PdfFactory pdf = new PdfFactory(request);
            pdf.CreateLabel();

            var stream = new FileStream(@"label.pdf", FileMode.Open);
            return File(stream, "application/pdf", "Label.pdf");       
        }
    }
}
