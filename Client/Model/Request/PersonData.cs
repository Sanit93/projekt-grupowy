﻿

namespace LabelBackend.Model
{
    public class PersonData
    {
        public PersonData()
        {
            FirstName = null;
            LastName = null;
            AddressData = new AddressData();
            PhoneNumber = null;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public AddressData AddressData { get; set; }
    }
}
