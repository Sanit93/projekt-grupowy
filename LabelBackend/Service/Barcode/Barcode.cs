﻿using System.Drawing;

namespace LabelBackend.Service.Barcode
{
    public class Barcode
    {
        BarcodeLib.Barcode barcode;
        string code;

        public Barcode(string code)
        {
            barcode = new BarcodeLib.Barcode(code);
            this.code = code;
        }

        public string GetCode()
        {
            return barcode.RawData;
        }

        public Image GetImage()
        {
            Image img = barcode.Encode(BarcodeLib.TYPE.TELEPEN, code, Color.Black, Color.White, 200, 60);
            return img;
        }
    }
}
