﻿

namespace LabelBackend.Model
{
    public class AddressData
    {
        public AddressData()
        {
            Street = null;
            HomeNumber = null;
            PostalCode = null;
            City = null;
            Country = null;
        }

        public string Street { get; set; }

        public string HomeNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
