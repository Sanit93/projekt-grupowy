﻿

namespace LabelBackend.Model
{
    public class RequestData
    {
        public PersonData SenderData { get; set; }
        public PersonData ReceiverData { get; set; }
    }
}
