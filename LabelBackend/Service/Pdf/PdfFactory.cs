﻿using LabelBackend.Model;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Drawing;
using System.IO;

namespace LabelBackend.Service.Pdf
{
    public class PdfFactory
    {
        PdfDocument document;
        RequestData request;
        XGraphics gfx;
        PdfPage page;
        Barcode.Barcode barcode;
        static int id;
        

        XFont fontBold = new XFont("Times new roman", 10, XFontStyle.Bold);
        XFont fontCode = new XFont("Times new roman", 10, XFontStyle.Regular);

        public PdfFactory(RequestData request)
        {
            id++;
            document = new PdfDocument();
            barcode = new Barcode.Barcode(id.ToString());
            this.request = request;
        }

        public void CreateLabel()
        {
            page = document.AddPage();
            page.Width = "11,2 cm";
            page.Height = "15,5 cm";
            gfx = XGraphics.FromPdfPage(page);
            DrawLines();
            SenderData();
            ReceiverData();
            DrawBarcode();
            DrawCode();


            document.Save("label.pdf");
        }

        void DrawLines()
        {
            XPen line = new XPen(XColors.Black, 1);
            gfx.DrawLine(line, 0, 120, page.Width, 120);
            gfx.DrawLine(line, page.Width / 2, 0, page.Width / 2, 120);
            gfx.DrawLine(line, 0, 350, page.Width, 350);
        }
        void SenderData()
        {

            gfx.DrawString($"FROM :", fontBold, XBrushes.Black, new XRect(20, 28, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.FirstName, fontBold, XBrushes.Black, new XRect(20, 38, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.LastName, fontBold, XBrushes.Black, new XRect(20, 46, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.PhoneNumber.ToString(), fontBold, XBrushes.Black, new XRect(20, 54, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.AddressData.Street, fontBold, XBrushes.Black, new XRect(20, 62, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.AddressData.HomeNumber, fontBold, XBrushes.Black, new XRect(20, 70, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.AddressData.PostalCode, fontBold, XBrushes.Black, new XRect(20, 78, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.AddressData.City, fontBold, XBrushes.Black, new XRect(20, 86, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.SenderData.AddressData.Country, fontBold, XBrushes.Black, new XRect(20, 94, page.Width, page.Height), XStringFormat.TopLeft);
           

        }
        void ReceiverData()
        {

            gfx.DrawString($"TO :", fontBold, XBrushes.Black, new XRect(180, 28, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.FirstName, fontBold, XBrushes.Black, new XRect(180, 38, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.LastName, fontBold, XBrushes.Black, new XRect(180, 46, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.PhoneNumber.ToString(), fontBold, XBrushes.Black, new XRect(180, 54, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.AddressData.Street, fontBold, XBrushes.Black, new XRect(180, 62, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.AddressData.HomeNumber, fontBold, XBrushes.Black, new XRect(180, 70, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.AddressData.PostalCode, fontBold, XBrushes.Black, new XRect(180, 78, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.AddressData.City, fontBold, XBrushes.Black, new XRect(180, 86, page.Width, page.Height), XStringFormat.TopLeft);
            gfx.DrawString(request.ReceiverData.AddressData.Country, fontBold, XBrushes.Black, new XRect(180, 94, page.Width, page.Height), XStringFormat.TopLeft);

        }

        void DrawBarcode()
        {
            Image img = barcode.GetImage();
            MemoryStream strm = new MemoryStream();
            img.Save(strm, System.Drawing.Imaging.ImageFormat.Png);

            XImage xfoto = XImage.FromStream(strm);
            gfx.DrawImage(xfoto, 25, 360);
        }
        void DrawCode()
        {
            gfx.DrawString(barcode.GetCode(), fontCode, XBrushes.Black, new XRect(45, 406, page.Width, page.Height), XStringFormat.TopLeft);
        }
    }
}
